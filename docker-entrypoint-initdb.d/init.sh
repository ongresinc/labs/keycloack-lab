#!/bin/bash

set -e

cat > /var/lib/postgresql/data/postgresql.conf << EOFCONF

# Adding listen_addresses
listen_addresses = '*'

EOFCONF