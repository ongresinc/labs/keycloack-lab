# keycloack-lab

This lab intends to multi-master Keycloack databases.

- [ ] Through LR?
  - [ ] Cross database replication and FWD to view cross-region users?
  - [ ] DB-in-place writes (check Keycloack database design)


Stated in the [Keycloack Cross-Data center documentation](https://www.keycloak.org/2017/09/cross-datacenter-support-in-keycloak.html):

> Database
> Keycloak uses RDBMS to persist some metadata about realms, clients, users etc. In cross-datacenter setup, we assume that either both datacenters talk to same database or every datacenter has it's own database, but both databases are synchronously replicated. In other words, when Keycloak server in site 1 persists any data and transaction is commited, those data are immediatelly visible by subsequent DB transactions on site 2.
> Details of DB setup are out-of-scope of Keycloak, however note that many RDBMS vendors like PostgreSQL or MariaDB offers replicated databases and synchronous replication. Databases are not shown in the example picture above just to make it a bit simpler.


https://hub.docker.com/r/jboss/keycloak/

Doc: https://github.com/keycloak/keycloak-containers/blob/15.0.2/server/README.md

Compose example: https://github.com/keycloak/keycloak-containers/blob/15.0.2/docker-compose-examples/keycloak-postgres.yml#L10
Docker: https://github.com/keycloak/keycloak-containers/tree/15.0.2/server


## Init

```bash
docker-compose up
```