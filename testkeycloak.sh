# Define network
docker network create keycloak-network

# just in case, stop two dbs
docker stop pg5432
docker stop pg5433

#run primary pg
docker run --rm --name pg5432 -e POSTGRES_PASSWORD=daf -e POSTGRES_INITDB_ARGS="--data-checksums" \
       -d            \
       -p 5432:5432  \
       -v /home/daf/temp/pgtest:/var/lib/postgresql/data \
       -v /home/daf/temp/logs:/logs \
       --net keycloak-network \
       postgres:alpine postgres -c port=5432

# find the IP of the db
# docker exec -it pg5432 bash
# ip a
# ... 172.20.0.2 /16 ....

# connect wui to primary db
docker run -p 8080:8080       \
    -e KEYCLOAK_USER=daf      \
    -e KEYCLOAK_PASSWORD=daf  \
    -e DB_VENDOR=postgres     \
    -e DB_ADDR=172.20.0.2     \
    -e DB_PORT=5432           \
    -e DB_DATABASE=keycloak   \
    -e DB_USER=postgres       \
    -e DB_PASSWORD=daf        \
    --net keycloak-network    \
    jboss/keycloak

## copy data folder

#run a 2nd pg, identically, overwriting port
docker run --rm --name pg5433 -e POSTGRES_PASSWORD=daf -e POSTGRES_INITDB_ARGS="--data-checksums" \
       -d        \
       -p 5433:5433  \
       -v /home/daf/temp/pgtest2:/var/lib/postgresql/data \
       -v /home/daf/temp/logs:/logs \
       --net keycloak-network \
       postgres:alpine postgres -c port=5433 #-c logging_collector=on -c log_destination=stderr -c log_directory=/logs

# find the IP of the db
# docker exec -it pg5433 bash
# ip a
# ... 172.20.0.3 /16 ....


/*  PRIMARY  */

SHOW wal_level;
CREATE PUBLICATION prueba FOR ALL TABLES;


select * from pg_replication_slots;


/*  REPLICA  */

CREATE SUBSCRIPTION prueba
CONNECTION 'host=172.20.0.2 port=5432 dbname=keycloak password=daf' 
PUBLICATION prueba
WITH (copy_data=false);

select * from  public.keycloak_group

CREATE PUBLICATION prueba2 FOR ALL TABLES;

/*  PRIMARY  */

CREATE SUBSCRIPTION prueba2
CONNECTION 'host=172.20.0.3 port=5433 dbname=keycloak password=daf' 
PUBLICATION prueba2
WITH (copy_data=false);

	
# connect 2nd wui to replica db, map other port
docker run -p 8090:8080       \
    -e KEYCLOAK_USER=daf      \
    -e KEYCLOAK_PASSWORD=daf  \
    -e DB_VENDOR=postgres     \
    -e DB_ADDR=172.20.0.3     \
    -e DB_PORT=5433           \
    -e DB_DATABASE=keycloak   \
    -e DB_USER=postgres       \
    -e DB_PASSWORD=daf        \
    --net keycloak-network    \
    jboss/keycloak
